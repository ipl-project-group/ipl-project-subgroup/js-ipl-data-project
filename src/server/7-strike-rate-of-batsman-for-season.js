const strikeRateOfBatsmanInEachSeason = (deliveries,matches) => {
    try {
        const seasonsArray = new Set(matches.map((match) => match.season))
        const batsmanStrikeRateInEachSeason={}
        seasonsArray.forEach((season)=>{
            const batsmanScoreDetails = {}
            matches.forEach((match)=>{
                if (match.season === season) {
                    deliveries.map((delivery)=>{
                        if(delivery.match_id === match.id){
                            if(delivery.batsman in batsmanScoreDetails){
                                batsmanScoreDetails[delivery.batsman].runs += Number(delivery.total_runs)
                                batsmanScoreDetails[delivery.batsman].balls +=1
                            }else{
                                batsmanScoreDetails[delivery.batsman] = {
                                    runs: Number(delivery.total_runs),
                                    balls:1
                                }
                            }
                        }
                    })
                }
            })

            const batsmanStrikeRate = {} 
            Object.keys(batsmanScoreDetails).map(batsman => {
                batsmanStrikeRate[batsman]=Number(((batsmanScoreDetails[batsman].runs/ batsmanScoreDetails[batsman].balls)*100).toFixed(2))
            })
            
            batsmanStrikeRateInEachSeason[season] = batsmanStrikeRate
            
        })
        
        return batsmanStrikeRateInEachSeason
    } catch (error) {
        console.log(error.message)
    }
}

module.exports = strikeRateOfBatsmanInEachSeason;