const playerWithHighestPlayerOfTheMatchPerSeason = (matches) => {
    try {
        const playerOfTheMatch = []
        const seasonsArray = new Set(matches.map((match)=>match.season))

        seasonsArray.forEach((year) => {
            const awardsInSeason = {}
            matches.forEach((match)=>{
                if (match.season === year) {
                    if (match.player_of_match in awardsInSeason) {
                        awardsInSeason[match.player_of_match].awards += 1
                    } else {
                        awardsInSeason[match.player_of_match] = {
                            playerName: match.player_of_match,
                            awards : 1
                        }
                    }
                }
            })
            playerOfTheMatch.push(awardsInSeason)
        })

        const playersWithHighestAwards = []

        playerOfTheMatch.forEach((season)=>{
            const playersObjects = Object.values(season)
            const player = playersObjects.sort((firstObject,secondObject)=>secondObject.awards-firstObject.awards)
            playersWithHighestAwards.push(player.slice(0,1))
        })
        return playersWithHighestAwards
    } catch (error) {
        console.log(error.message)
    }
}



module.exports = playerWithHighestPlayerOfTheMatchPerSeason;