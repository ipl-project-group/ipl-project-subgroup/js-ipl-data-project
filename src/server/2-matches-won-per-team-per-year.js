const matchesWonPerTeamPerYear = (matches) => {
    const error = new Error("Something went wrong in the code")
    try {
        const matchesWon = {
            "2008" : {},
            "2009" : {},
            "2010" : {},
            "2011" : {},
            "2012" : {},
            "2013" : {},
            "2014" : {},
            "2015" : {},
            "2016" : {},
            "2017" : {}
        }

        matches.forEach(match => {
            if (match.season) {
                if (match.winner !=="") {
                    if (match.winner in matchesWon[match.season]) {
                        matchesWon[match.season][match.winner] += 1
                    } else {
                        matchesWon[match.season][match.winner] = 1
                    }
                }
            }
        });
        
        return matchesWon

    } catch (error) {
        console.log(error.message)
    }
}



module.exports = matchesWonPerTeamPerYear;