const bowlerWithBestEconomyInSuperOver = (deliveries) => {
    
    try {
        const superOversDeliveries = deliveries.filter((delivery)=>delivery.is_super_over === "1")
        const bowlersWithEconomy={};
        superOversDeliveries.forEach((innings)=>{
            if((innings.bowler in bowlersWithEconomy)){
                bowlersWithEconomy[innings.bowler].balls += 1
                bowlersWithEconomy[innings.bowler].runs += parseInt(innings.total_runs)
            }else{
                bowlersWithEconomy[innings.bowler] = {
                    'balls':1,
                    'runs':parseInt(innings.total_runs)
                }
            }
        })

        const bowlersEconomicalData = Object.keys(bowlersWithEconomy).map((bowler)=>{
            return {
                bowler,
                'economy':(bowlersWithEconomy[bowler].runs/(bowlersWithEconomy[bowler].balls/6)).toFixed(3)};
        })

        return bowlersEconomicalData.sort((a,b)=> a.economy-b.economy).slice(0,1)
    } catch (error) {
        console.log(error.message)
    }
}
module.exports = bowlerWithBestEconomyInSuperOver