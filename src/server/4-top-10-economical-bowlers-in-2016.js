const topTenEconomicalBowlersIn2015 = (deliveries,matches,year) => {
    try {
        const matchesBySeason=[]
        
        matches.forEach((match)=>{
            if (match.season === year) {
                deliveries.forEach((delivery)=>{
                    if(delivery.match_id === match.id) {
                        matchesBySeason.push(delivery)
                    }
                })
            }
        })
        
        const bowlersOverDetails={};

        matchesBySeason.forEach((innings)=>{
            if((innings.bowler in bowlersOverDetails)){
                bowlersOverDetails[innings.bowler].balls += 1
                bowlersOverDetails[innings.bowler].runs += parseInt(innings.total_runs)
            }else{
                bowlersOverDetails[innings.bowler] = {
                    'balls':1,
                    'runs':parseInt(innings.total_runs)
                }
            }
        })

        const bowlersEconomicalData = Object.keys(bowlersOverDetails).map((bowler)=>{
            return {
                bowler,
                'economy':(bowlersOverDetails[bowler].runs/(bowlersOverDetails[bowler].balls/6)).toFixed(3)
            }
        })

        return bowlersEconomicalData.sort((a,b)=> a.economy-b.economy).slice(0,10)
    } catch (error) {
        console.log(error.message)
    }

}
module.exports = topTenEconomicalBowlersIn2015