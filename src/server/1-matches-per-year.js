const macthesPlayedPerYear = (matches) => {
    try {
        const matchesPlayedInEachYear = {}
        matches.map((match)=>{
            if (match.season in matchesPlayedInEachYear) {
                matchesPlayedInEachYear[match.season] += 1
            } else {
                matchesPlayedInEachYear[match.season] = 1
            }
        })
        return matchesPlayedInEachYear
    } catch (error) {
        console.log(error.message)
    }
}

module.exports = macthesPlayedPerYear