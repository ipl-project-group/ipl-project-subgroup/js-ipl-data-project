const extraRunsConceededPerTeamIN2016 = (deliveriesData,matchesData,year) => {
    try {
        const deliveriesInParticularSeason = []
        matchesData.forEach((match) => {
            if (match.season === year){
                deliveriesData.forEach((delivery)=>{
                    if (delivery.match_id === match.id) {
                        deliveriesInParticularSeason.push(delivery);
                    }
                })
            }
        })
    
        const extraRunsConceeded = {}
        deliveriesInParticularSeason.forEach((eachDelivery)=>{
            if (eachDelivery.bowling_team in extraRunsConceeded) {
                extraRunsConceeded[eachDelivery.bowling_team].extraRuns += parseInt(eachDelivery.extra_runs)
            } else {
                extraRunsConceeded[eachDelivery.bowling_team] = {
                    extraRuns : parseInt(eachDelivery.extra_runs)
                }
            }
        })
        return extraRunsConceeded
    } catch (error) {
        console.log(error.message)
    }
}

module.exports = extraRunsConceededPerTeamIN2016;