document.addEventListener("DOMContentLoaded",() => {
    fetch('./output/matchesPlayedPerYear.json')
     .then((response) => response.json())
     .then((data) => {
         matchesPlayedPerYear(data)
     })
 
 
     function matchesPlayedPerYear(data) {
        const seasons = Object.keys(data["matchesPerSeason"])
        const  matches = Object.values(data["matchesPerSeason"])
 
        Highcharts.chart("container", {
             chart : {
                 type: "column",
             },
             title : {
                 text: "Matches played per season",
             },
             xAxis: {
                 title: {
                     text: "Seasons",
                     style: {
                         fontSize:"12px",
                         color: "#000000"
                     }
                 },
             labels: {
                 style: {
                     fontSize:"12px",
                     color: "#000000"
                 }
             },
             categories: seasons
             },
             yAxis: {
                 title: {
                     text: "matchesPlayedInEachSeason",
                     style: {
                         fontSize:"12px",
                         color: "#000000"
                     },
                 },
             labels: {
                 style: {
                     fontSize:"12px",
                     color: "#000000"
                 }
             },
         },
         series: [{
             data: matches,
               dataLabels: {
                 enabled: true,
                 rotation: 0,
                 color: '#FFFFFF',
                 y: 20, // 10 pixels down from the top
                 style: {
                     fontSize: '12px',
                     fontFamily: 'Verdana, sans-serif',
                 }
             }
         }],
         
 
     })
 
     }
 });
 
 document.addEventListener("DOMContentLoaded",() => {
     fetch('./output/extraRunsConceededPerTeamIn2016.json')
     .then((response) => response.json())
     .then((data) => {
         extraRunsConceededIn2016(data)
     })
 
     function extraRunsConceededIn2016(data) {
         const teamNames = Object.keys(data["extraRunsConceededPerTeamIN2016"])
         const extraRuns = Object.values(data["extraRunsConceededPerTeamIN2016"])
         const runs = extraRuns.map((run)=>run.extraRuns)
 
         Highcharts.chart("extraruns", {
             chart : {
                 type: "column",
             },
             title : {
                 text: "Extra runs conceeded in 2016 season."
             },
             xAxis: {
                 title: {
                     text: "TeamNames",                
                     style: {
                         fontSize:"12px",
                         color: "#000000"
                     }
                 },
                 labels: {
                     style: {
                         fontSize:"12px",
                         color: "#000000"
                     }
                 },
                 categories: teamNames
             },
             yAxis: {
                 title: {
                     text: "extraRunsConceededin2016",
                     style: {
                         fontSize:"12px",
                         color: "#000000"
                     }
                 },
                 labels: {
                     style: {
                         fontSize:"12px",
                         color: "#000000"
                     }
                 },
             },
             series: [{
                 data: runs,
                   dataLabels: {
                     enabled: true,
                     rotation: 0,
                     color: '#FFFFFF',
                     y: 20, // 10 pixels down from the top
                     style: {
                         fontSize: '12px',
                         fontFamily: 'Verdana, sans-serif',
                     },
                 }
                   
             }],
             
     
         })
     };
     
 })
 
 document.addEventListener("DOMContentLoaded",() => {
     fetch('./output/numberOfTimesTeamWhichWonBothTossAndMatch.json')
     .then((response) => response.json())
     .then((data) => {
         teamsWhichWonBothMatchAndToss(data)
     })
 
     function teamsWhichWonBothMatchAndToss(data) {
 
         const teamNames = Object.keys(data["teamWhichWonBothTeamAndatch"])
         const numberOfTimes = Object.values(data["teamWhichWonBothTeamAndatch"])
 
         Highcharts.chart("wonTossAndMatch", {
             chart : {
                 type: "column",
             },
             title : {
                 text: "Matches count which won both toss and match."
             },
             xAxis: {
                 title: {
                     text: "TeamNames",
                     style: {
                         fontSize:"12px",
                         color: "#000000"
                     }
                 },
                 labels: {
                     style: {
                         fontSize:"12px",
                         color: "#000000"
                     }
                 },
                 categories: teamNames
             },
             yAxis: {
                 title: {
                     text: "countOfWins",
                     
                     style: {
                         fontSize:"12px",
                         color: "#000000"
                     }
                 },
                 labels: {
                     style: {
                         fontSize:"12px",
                         color: "#000000"
                     }
                 },
             },
             series: [{
                 data: numberOfTimes,
                  dataLabels: {
                     enabled: true,
                     rotation: 0,
                     color: '#FFFFFF',
                     y: 20, // 10 pixels down from the top
                     style: {
                         fontSize: '12px',
                         fontFamily: 'Verdana, sans-serif',
                     },
                 }
             }],
             
     
         })
 
     }
     
 });
 
 document.addEventListener("DOMContentLoaded",() => {
     fetch('./output/topTenEconomicalBowlerIn2015.json')
         .then((response) => response.json())
         .then((data) => economicalBowlers(data))
 
     function economicalBowlers(data) {
         const bowlers = data.topTenEconominalBowlersIn2015.map((eachObject) => eachObject.bowler)
         const economy = data.topTenEconominalBowlersIn2015.map((eachObject) => eachObject.economy).map((economy)=>Number(economy))
         
         Highcharts.chart("economicalBowlers", {
             chart : {
                 type: "column",
             },
             title : {
                 text: "Top 10 best economical bowlers in 2015 season."
             },
             xAxis: {
                 title: {
                     text: "BowlerNames",
                     style: {
                         fontSize:"12px",
                         color: "#000000"
                     },
                 },
                 labels: {
                     style: {
                         fontSize:"12px",
                         color: "#000000"
                     }
                 },
                 categories: bowlers
             },
             yAxis: {
                 title: {
                     text: "economy",
                     style: {
                         fontSize:"12px",
                         color: "#000000"
                     },
                 },
                 labels: {
                     style: {
                         fontSize:"12px",
                         color: "#000000"
                     }
                 },
             },
             series: [{
                 data: economy,
                   dataLabels: {
                     enabled: true,
                     rotation: 0,
                     color: '#FFFFFF',
                     y: 20, // 10 pixels down from the top
                     style: {
                         fontSize: '12px',
                         fontFamily: 'Verdana, sans-serif',
                     },
                 }
             }]
     
         })
 
     }
 });
 
 document.addEventListener("DOMContentLoaded",() => {
     fetch('./output/playerWithHighestNumberOfPlayerOfTheMatchPerSeason.json')
         .then((response) => response.json())
         .then((data) => {
             economicalBowlers(data.flat(1))
         })
 
     function economicalBowlers(data) {
         const players = data.map((playerObject) => playerObject.playerName)
         const awards = data.map((playerObject) => playerObject.awards)
         
         Highcharts.chart("highestAwards", {
             chart : {
                 type: "column",
             },
             title : {
                 text: "Players with highest number of Player of Match Awards."
             },
             xAxis: {
                 title: {
                     text: "PlayerNames",
                     style: {
                         fontSize:"12px",
                         color: "#000000"
                     }
                 },
                 categories: players,
                 labels: {
                     style: {
                         fontSize:"12px",
                         color: "#000000"
                     }
                 }
             },
             yAxis: {
                 title: {
                     text: "awards",            
                     style: {
                         fontSize:"12px",
                         color: "#000000"
                     }
                 
                 },
                 labels: {
                     style: {
                         fontSize:"12px",
                         color: "#000000"
                     }
                 },
             },
             series: [{
                 data: awards,
                   dataLabels: {
                     enabled: true,
                     rotation: 0,
                     color: '#FFFFFF',
                     y: 20, // 10 pixels down from the top
                     style: {
                         fontSize: '12px',
                         fontFamily: 'Verdana, sans-serif',
                     },
                 }
             }]
     
         })
 
     }
 });
 
 document.addEventListener("DOMContentLoaded",() => {
     fetch('./output/matchesWonPerTeamPerYear.json')
         .then((response) => response.json())
         .then((data) => {
             matchesWonByTeamPerSeason(data)
         })
 
     function matchesWonByTeamPerSeason(data) {
         const seasons = [];
         const teamNames = [];
         for (let key in data) {
             seasons.push(key)
             for (let teamName in data[key]) {
                 if (!teamNames.includes(teamName)) {
                     teamNames.push(teamName);
                   }
               }
           }
           let seasonsData = [];
           for (let teamName in teamNames) {
               let arr = [];
               for (let season in seasons) {
                   if (data[seasons[season]].hasOwnProperty(teamNames[teamName])) {
                       arr.push(data[seasons[season]][teamNames[season]]);
                   }
                   else {
                       arr.push(0);
                   }
               }
               let obj = {
                   'name': teamNames[teamName],
                   'data': arr
               }
               seasonsData.push(obj);
           }
 
         Highcharts.chart("matchesWonByTeanInEachSeason", {
             chart : {
                 type: "column",
             },
             title : {
                 text: "Chart-5"
             },
             xAxis: {
                 title: {
                     text: "BowlerNames",
                     labels: {
                         style: {
                             fontSize:"12px",
                             color: "#000000"
                         }
                     },
                 },
                 labels: {
                     style: {
                         fontSize:"12px",
                         color: "#000000"
                     }
                 },
                 categories: seasons
             },
             yAxis: {
                 title: {
                     text: "economy",
                     labels: {
                         style: {
                             fontSize:"12px",
                             color: "#000000"
                         }
                     },
                 },
                 labels: {
                     style: {
                         fontSize:"12px",
                         color: "#000000"
                     }
                 },
             },
             series: [{
                 data: seasonsData,
                   dataLabels: {
                     enabled: true,
                     rotation: 0,
                     color: '#FFFFFF',
                     y: 20, // 10 pixels down from the top
                     style: {
                         fontSize: '12px',
                         fontFamily: 'Verdana, sans-serif',
                     },
                 }
             }]
     
         })
 
     }
 });
 
 document.addEventListener("DOMContentLoaded",() => {
     fetch('./output/matchesWonPerTeamPerYear.json')
         .then((response) => response.json())
         .then((data) => {
             console.log(data)
             matchesWonByTeamPerSeason(data)
         })
 
     function matchesWonByTeamPerSeason(data) {
         const seasons = [];
         const teamNames = [];
         for (let key in data) {
             seasons.push(key)
             for (let teamName in data[key]) {
                 if (!teamNames.includes(teamName)) {
                     teamNames.push(teamName);
                   }
               }
           }
           const seasonsData = [];
           for (let teamName in teamNames) {
               let arr = [];
               for (let season in seasons) {
                   if (data[seasons[season]].hasOwnProperty(teamNames[teamName])) {
                       arr.push(data[seasons[season]][teamNames[teamName]]);
                   }
                   else {
                       arr.push(0);
                   }
               }
               let obj = {
                   'name': teamNames[teamName],
                   'data': arr
               }
               seasonsData.push(obj);
           }
 
           Highcharts.chart("matchesWonByTeanInEachSeason", {
             chart: {
                 type: 'column'
             },
             title: {
                 text: "Matches won by teams in each season."
             },
             xAxis: {
                 categories: seasons,
                 crosshair: true,
                 title: {
                     text: "seasons",
                     style: {
                         fontSize:"12px",
                         color: "#000000"
                     }
                 },
                 labels: {
                     style: {
                         fontSize:"12px",
                         color: "#000000"
                     }
                 }
             },
             yAxis: {
                 min: 0,
                 title: {
                     text: "Matches Won",
                     
                     style: {
                         fontSize:"12px",
                         color: "#000000"
                     }
                 }
                 ,
             labels: {
                 style: {
                     fontSize:"12px",
                     color: "#000000"
                 }
             },
             },
             tooltip: {
                 headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                 pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                     '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                 footerFormat: '</table>',
                 shared: true,
                 useHTML: true
             },
             plotOptions: {
                 column: {
                     pointPadding: 0.2,
                     borderWidth: 0
                 }
             },
             series: seasonsData,
         });
     }
 });
 
 document.addEventListener("DOMContentLoaded",() => {
     fetch('./output/strikeRateOfBatsmanInEachSeason.json')
         .then((response) => response.json())
         .then((data) => {
             strikeRate(data)
         })
 
     function strikeRate(data) {
         const seasons = [];
         const batsmanData = [];
         for (let key in data) {
             seasons.push(key)
             for (let batsman in data[key]) {
                 if (!batsmanData.includes(batsman)) {
                     batsmanData.push(batsman);
                   }
               }
           }
           const seasonsData = [];
           for (let batsman in batsmanData) {
               let arr = [];
               for (let season in seasons) {
                   if (data[seasons[season]].hasOwnProperty(batsmanData[batsman])) {
                       arr.push(data[seasons[season]][batsmanData[batsman]]);
                   }
                   else {
                       arr.push(0);
                   }
               }
               let obj = {
                   'name': batsmanData[batsman],
                   'data': arr
               }
               seasonsData.push(obj);
           }
           console.log(seasonsData)
 
           Highcharts.chart("strikeRate", {
             chart: {
                 type: 'column'
             },
             title: {
                 text: "Strike Rate of each batsmen in each season."
             },
             xAxis: {
                 categories: seasons,
                 crosshair: true,
                 title: {
                     text: "seasons",
                     style: {
                         fontSize:"12px",
                         color: "#000000"
                     }
                 },
                 labels: {
                     style: {
                         fontSize:"12px",
                         color: "#000000"
                     }
                 },
             },
             yAxis: {
                 min: 0,
                 title: {
                     text: "StrikeRate",            
                     style: {
                         fontSize:"12px",
                         color: "#000000"
                     }
                 },
                 labels: {
                     style: {
                         fontSize:"12px",
                         color: "#000000"
                     }
                 },
             },
             tooltip: {
                 headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                 pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                     '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                 footerFormat: '</table>',
                 shared: true,
                 useHTML: true
             },
             plotOptions: {
                 column: {
                     pointPadding: 0.2,
                     borderWidth: 0
                 }
             },
             series: seasonsData
         });
 
     }
 });