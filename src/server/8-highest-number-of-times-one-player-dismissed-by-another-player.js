const playersDismissedByAnotherPlayer = (deliveries) => {
    try {
        const dismissalObjectOfPlayer = {}

        deliveries.forEach((delivery)=>{
            if (delivery.player_dismissed !== "") {
                if (delivery.player_dismissed in dismissalObjectOfPlayer) {
                    if (delivery.bowler in dismissalObjectOfPlayer[delivery.player_dismissed]) {
                        dismissalObjectOfPlayer[delivery.player_dismissed][delivery.bowler] += 1
                    } else {
                        dismissalObjectOfPlayer[delivery.player_dismissed][delivery.bowler] = 1
                    }
                }else {
                    dismissalObjectOfPlayer[delivery.player_dismissed] = {
                        [delivery.bowler] : 1
                    }
                }
            }
        })

        console.log(dismissalObjectOfPlayer)
        const dismissalCounts = Object.keys(dismissalObjectOfPlayer).map((dismissedBatsman) =>{
           //console.log(dismissedBatsman)
            const sortedDismissalCounts = Object.entries(dismissalObjectOfPlayer[dismissedBatsman])
            //console.log(sortedDismissalCounts)
            const sortedCounts = sortedDismissalCounts.sort((firstObject,secondObject)=>secondObject[1]-firstObject[1]).slice(0,1).flat(1)
            return [dismissedBatsman,sortedCounts]
        });
        //console.log(dismissalCounts)
        //console.log(dismissalCounts)
        const mostDismissedPlayer = dismissalCounts.sort((firstObject,secondObject) => secondObject[1][1]-firstObject[1][1])


        return mostDismissedPlayer[0]
    }catch(error) {
        console.log(error.message)
    }

}

module.exports = playersDismissedByAnotherPlayer
