const teamWonBothTossAndMatch = (matches) => {
    try {
        const teamWhichWonBothTossAndMatch = {}
        matches.forEach((match)=>{
            if (match.toss_winner === match.winner) {
                if (match.winner in teamWhichWonBothTossAndMatch) {
                    teamWhichWonBothTossAndMatch[match.winner] += 1
                } else {
                    teamWhichWonBothTossAndMatch[match.winner] = 1
                }
            }
        })
        return teamWhichWonBothTossAndMatch
    } catch (error) {
        return error.message
    }
}

module.exports = teamWonBothTossAndMatch;